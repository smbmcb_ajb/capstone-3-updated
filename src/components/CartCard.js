import {Container, Row, Table} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import Swal from 'sweetalert2'

export default function CartCard({product}){
    // console.log(product)
   
    
    const [quantity, setQuantity] = useState(product.quantity)
    
    useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/users/cart/${product.productId}/change-quantity/${quantity}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
	},[quantity])

    const decrementQuantity = () => {
        if(quantity > 1){
            setQuantity(quantity-1)
        }
    }


    const changeQuantity = (e) => {
        setQuantity(e.target.value)
        // console.log(quantity)
    }

    const removeItem = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/cart/${product.productId}/remove-item`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result.status === "success"){
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: result.message
                })
            } else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: result.message
                })
            }
        })
    }


return(
    <Container style={{height: '100%'}}>
            <Row style={{ height: '28%'}}>
                
            </Row>
            <Row>
            <Table>
                
                <tbody style={{boxShadow: '0px 2px 10px 0px gray'}}>
                <tr style={{fontSize:'18px', borderTopStyle: 'hidden', borderBottomStyle: 'hidden'}}>
                        <td style={{width:'44%'}}>{product.name}</td>
                        <td className='text-center' style={{width:'14%'}}>{product.price}</td>
                        <td className='text-center' style={{width:'14%'}}>
                            <div className='cart-quantity-component'>
                                <button onClick={decrementQuantity} className='cart-btn-ctr'>-</button>
                                    <input className='cart-input-quantity' type="number" value={quantity} onChange={changeQuantity}/>
                                <button onClick={() => setQuantity(quantity+1)} className='cart-btn-ctr'>+</button>
                            </div>
                        </td>
                        <td className='text-center' style={{width:'14%', color: 'red'}}>₱{product.subTotal}</td>
                        <td className='text-center' style={{width:'14%'}}><span className='remove-item' onClick={removeItem}>Remove</span></td>
                </tr>
                </tbody>
                
               
                </Table>
            </Row>
                
        </Container>  
)
}
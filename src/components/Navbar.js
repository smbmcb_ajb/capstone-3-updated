import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Link, NavLink} from 'react-router-dom'
import Form from 'react-bootstrap/Form';
import {useState, useContext, useEffect} from 'react'
import UserContext from '../UserContext'
import NavDropdown from 'react-bootstrap/NavDropdown';




function NavigationBar() {
    const [cartCount, setCartCount] = useState([])
    const [color, setColor] = useState('')

    const [show, setShow] = useState('none')
  
  const chatCollapse = () => {
    document.querySelector("#textarea").focus()
    if(show === 'none'){
      setShow('flex')
      
      
    } else {
      setShow('none')
    }
    document.getElementById('incoming-message').scrollIntoView(false)
  }


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
            // console.log(result)
      if(localStorage.getItem('token')){
            setCartCount( result.cart.products.map(prod => {
                return result.cart.products.length
            }))
        
        
      }
			
		})
	
  })  
//   console.log(cartCount.length)

  useEffect(()=> {
    if(cartCount.length > 0){
        setColor('red')
    } else {
        setColor('white')
    }
  }, [cartCount.length])

//   console.log(color)
   
    const [userType, setUserType] = useState('')

    const {user} = useContext(UserContext)

    useEffect(()=> {
        if(localStorage.getItem('admin') == "true") {
            setUserType('| Admin')
        } else {
            setUserType('')
        }  
    },[localStorage.getItem('admin')])

    return(
        <>
            <Navbar className='navbar-big' id="main-navbar" fixed="top" bg="light" variant="light" style={{ boxShadow: '0px 2px 10px 0px gray', height: '80px', fontSize: '22px'}}>
                <Container style={{width:'80%'}}>
                <img className = "nav-logo" src="/ecommerce.png" alt="company logo" />
                    <Nav.Link className='link-hover' as={NavLink} to="/">Online-Tindahan</Nav.Link>
                    <Nav className="me-auto">
                    
                    {
                        (localStorage.getItem('admin') === "true") ?
                        <>
                            <Nav.Link className='link-hover' as={NavLink} to="/">Home</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/admin/products">Products</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/admin/users">Users</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/admin/orders">Orders</Nav.Link>
                        </>
                        :
                        <>
                            <Nav.Link className='link-hover' as={NavLink} to="/">Home</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/products">Products</Nav.Link>
                            
                        </>
                    }

            
                    </Nav>
                    {
                        (localStorage.getItem('admin') === "false") ?
                        <Link to="/cart" style={{ textDecoration: 'none', color: 'black' }}>
                            <div style={{width:'100px'}} ><img style={{width:'35px', height: '30px', margin: '15px'}} src="/cart.png" alt="cart" />
                                {
                                    (cartCount.length > 0) ?
                                        <span className='nav-cart' style={{position: 'relative', paddingLeft: '5px', paddingRight: '5px', paddingBottom: '0px', paddingTop: '0px',  borderRadius:'20px', right:'25px', top: '-10px', width:'100%', fontSize:'18px', backgroundColor: 'red', color: 'white'}}>{cartCount.length}</span>
                                    :
                                        <></>
                                }
                                
                                
                            </div>
                        </Link>
                        :
                        <></>
                    }
                    
                    
                    {
                        localStorage.getItem('token') !== null ?
                        <>{localStorage.getItem('firstName')} {userType}
                        <NavDropdown title={<img style={{width:'30px', height: '30px', margin: '15px'}} src='/profile.png' alt='cart' />} id="basic-nav-dropdown">
                            <Link className='link-hover' to="/account" style={{ textDecoration: 'none', color: 'black', marginLeft:'10px', fontSize: '18px', paddingTop:'5px'}} onClick={() => localStorage.setItem('key', 'account')}>My Account</Link><br/>
                            {
                                (localStorage.getItem('admin') === "true") ?
                                    <></>
                                :
                                <Link className='link-hover' to="/account" style={{ textDecoration: 'none', color: 'black', marginLeft:'10px', fontSize: '18px', paddingTop:'10px' }} onClick={() => localStorage.setItem('key', 'orders')}>My Purchase</Link>
                            }
                            
                            <NavDropdown.Divider />
                            
                            <Link className='link-hover-red' to="/logout" style={{ textDecoration: 'none', color: 'black', marginLeft:'10px', fontSize: '18px', paddingTop: '10px' }}>Logout</Link>
                        
                        </NavDropdown>
                        </>
                        :
                        <NavLink style={{textDecoration:'none', color: 'black', fontWeight:'bold'}} to='/login'>
                            <button className='btn-login-nav'>Login</button>
                        </NavLink>

                    }
                    
                    
                </Container>
                
            </Navbar>
            
            <Navbar className='navbar-small' id="main-navbar" bg="light" expand="lg" fixed="top" style={{ boxShadow: '0px 2px 10px 0px gray', fontSize: '22px'}}>
            <Container>
            <img className = "nav-logo" src="/ecommerce.png" alt="company logo" />
                <NavLink className='link-hover' style={{marginRight:'auto', textDecoration:'none', color:'black'}} to="/">Online-Tindahan</NavLink>
                    {
                        (localStorage.getItem('admin') === "false") ?
                        <Link className='big-screen-cart' to="/cart" style={{ textDecoration: 'none', color: 'black' }}>
                            <div style={{width:'100px'}} ><img style={{width:'35px', height: '30px', margin: '15px'}} src="/cart.png" alt="cart" />
                                {
                                    (cartCount.length > 0) ?
                                        <span className='nav-cart' style={{position: 'relative', paddingLeft: '5px', paddingRight: '5px', paddingBottom: '0px', paddingTop: '0px',  borderRadius:'20px', right:'25px', top: '-10px', width:'100%', fontSize:'18px', backgroundColor: 'red', color: 'white'}}>{cartCount.length}</span>
                                    :
                                        <></>
                                }
                                
                                
                            </div>
                        </Link>
                        :
                        <></>
                    }
                    {
                        localStorage.getItem('token') !== null ?
                        <>{localStorage.getItem('firstName')} {userType}
                        <NavDropdown title={<img style={{width:'30px', height: '30px', margin: '15px'}} src='/profile.png' alt='cart' />} id="basic-nav-dropdown">
                            <Link className='link-hover' to="/account" style={{ textDecoration: 'none', color: 'black', marginLeft:'10px', fontSize: '18px', paddingTop:'5px'}} onClick={() => localStorage.setItem('key', 'account')}>My Account</Link><br/>
                            {
                                (localStorage.getItem('admin') === "true") ?
                                    <></>
                                :
                                <Link className='link-hover' to="/account" style={{ textDecoration: 'none', color: 'black', marginLeft:'10px', fontSize: '18px', paddingTop:'10px' }} onClick={() => localStorage.setItem('key', 'orders')}>My Purchase</Link>
                            }
                            
                            <NavDropdown.Divider />
                            
                            <Link className='link-hover-red' to="/logout" style={{ textDecoration: 'none', color: 'black', marginLeft:'10px', fontSize: '18px', paddingTop: '10px' }}>Logout</Link>
                        
                        </NavDropdown>
                        </>
                        :
                        <NavLink style={{textDecoration:'none', color: 'black', fontWeight:'bold'}} to='/login'>
                            <button className='btn-login-nav'>Login</button>
                        </NavLink>

                    }
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    {
                        (localStorage.getItem('admin') === "true") ?
                        <>
                            <Nav.Link className='link-hover' as={NavLink} to="/">Home</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/admin/products">Products</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/admin/users">Users</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/admin/orders">Orders</Nav.Link>
                        </>
                        :
                        <>
                            <Nav.Link className='link-hover' as={NavLink} to="/">Home</Nav.Link>
                            <Nav.Link className='link-hover' as={NavLink} to="/products">Products</Nav.Link>
                            
                        </>
                    }
                    
                </Nav>
                </Navbar.Collapse>
            </Container>
            </Navbar>
                    {
                        (localStorage.getItem('admin') === "false") ?
                        <Link className="small-screen-cart" to="/cart" style={{ textDecoration: 'none', color: 'black', position:'fixed', top:'90px',right:'1px', zIndex:'1', display:'none' }}>
                            <div style={{width:'70px'}} ><img style={{width:'30px', height: '30px', margin: '5px'}} src="/cart.png" alt="cart" />
                                {
                                    (cartCount.length > 0) ?
                                        <span className='nav-cart' style={{position: 'relative', paddingLeft: '5px', paddingRight: '5px', paddingBottom: '0px', paddingTop: '0px',  borderRadius:'20px', right:'17px', top: '-10px', width:'100%', fontSize:'18px', backgroundColor: 'red', color: 'white'}}>{cartCount.length}</span>
                                    :
                                        <></>
                                }
                                
                                
                            </div>
                        </Link>
                        :
                        <></>
                    }
        </>
    )
}
export default NavigationBar;
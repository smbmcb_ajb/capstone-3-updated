import {useState, useEffect} from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';
import {Link, NavLink} from 'react-router-dom'

export default function ProductCard({product}) {
  // Destructuring the props
  const {name, description, price, _id} = product

  // Using the state
  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0)
  const [slot, setSlot] = useState(15)
  const [isOpen, setIsOpen] = useState(true)

  
  return (
    <Link to={`/products/${_id}`} style={{ textDecoration: 'none', color: 'black' }}>
    <Card className="product-card text-center">
      <Card.Img variant="top" height='180' src="/image-cap.svg" />
      <Card.Body >
        <Card.Title>{name}</Card.Title>
        
        	<Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
        
        
        	<Card.Subtitle style={{color: 'red', fontSize:'20px'}}>₱{price}</Card.Subtitle>
        
        
        
      </Card.Body>
      <Card.Footer className = 'text-center details'>Click for details</Card.Footer>
    </Card>
    </Link>
  );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
// ProductCard.propTypes = {
//   course: PropTypes.shape({
//     name:PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }
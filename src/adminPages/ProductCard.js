import {useState, useEffect} from 'react';
import Button from 'react-bootstrap/Button';
import {Card, Container, Modal, Row, Table, Form} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link, NavLink} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function ProductCard({product}) {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [id, setId] = useState('')
    const [error, setError] = useState('')
    const [display, setDisplay] = useState('')
  // Destructuring the props
//   const {name, description, price, _id} = product

  // Using the state
  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0)
  const [slot, setSlot] = useState(15)
  const [isOpen, setIsOpen] = useState(true)

  const [show, setShow] = useState(false);

  useEffect(()=> {
    if(name !== "" || description !== "" || price !== ""){
        setDisplay("none")
    }
    },[name, description, price])

  const handleClose = () => setShow(false);
  const handleShow = (e) => {
        setShow(true)
        const buttonValue = e.target.value;
        setId(buttonValue)
        setName(product.name)
        setDescription(product.description)
        setPrice(product.price)
    };

const enableStock = (e) => {
    const buttonValue = e.target.value;
    fetch(`${process.env.REACT_APP_API_URL}/products/${buttonValue}/unarchive`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => response.json())
    .then(result => {
        if(result.status === "success"){
            
            Swal.fire({
                title: 'Success',
                icon: 'success',
                text: result.message
            })

        } else {
            setDisplay("flex")
            setError(result.message)
        }
    })
} 

const disableStock = (e) => {
    const buttonValue = e.target.value;
    fetch(`${process.env.REACT_APP_API_URL}/products/${buttonValue}/archive`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => response.json())
    .then(result => {
        if(result.status === "success"){
            
            Swal.fire({
                title: 'Success',
                icon: 'success',
                text: result.message
            })

        } else {
            setDisplay("flex")
            setError(result.message)
        }
    })
}    

const updateProduct = () => {

    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/update`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            name:name,
            description: description,
            price: price
        })
    })
    .then(response => response.json())
    .then(result => {
        if(result.status === "success"){
            
            Swal.fire({
                title: 'Success',
                icon: 'success',
                text: result.message
            })

            setName('')
            setDescription('')
            setPrice('')
        } else {
            setDisplay("flex")
            setError(result.message)
        }
    })
}

//   console.log(product.isActive)
  return (
    // <></>
    <Container>
        
    
       <Row>
        <Table striped >
                <tbody>
                
                    <tr className='admin-product-table' style={{fontSize:'16px'}}>
                        <td style={{width:'20%', wordBreak: 'break-all'}}>{product._id}</td>
                        <td style={{width:'15%'}}>{product.name}</td>
                        <td style={{width:'35%'}}>{product.description}</td>
                        <td style={{width:'10%'}}>{product.price}</td>
                        <td style={{width:'10%'}}>
                            {
                                (product.isActive === true) ?
                                    <h6>In Stock</h6>
                                :
                                    <h6 style={{color: 'red'}}>Out of Stock</h6>
                            }
                            </td>
                        <td style={{width:'10%'}}><Button className="admin-product-operation" value={product._id} variant="primary" onClick={handleShow} style={{width:'70%', fontSize:'12px', paddingY:'0',paddingX:'auto', textAlign:'center', display:'flex',justifyContent:'center'}}>Update</Button>

                            {
                                (product.isActive === true) ?
                                    <Button value={product._id} variant="danger" className="my-2 admin-product-operation" style={{width:'70%', fontSize:'12px', paddingY:'0',paddingX:'auto', textAlign:'center', display:'flex',justifyContent:'center'}} onClick={disableStock}>Disable</Button>
                                :
                                <Button value={product._id} variant="primary" className="my-2 admin-product-operation" style={{width:'80%', fontSize:'12px', paddingY:'0',paddingX:'auto', textAlign:'center', display:'flex',justifyContent:'center'}} onClick={enableStock}>Enable</Button>
                            }
                            
                        </td>
                    </tr>
                
                </tbody>
        
        </Table>
      </Row>
        
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <label className='errorMessage-container mb-3' style={{display:display}}>{error}</label>
            <Form.Floating className="mb-3">
                <Form.Control
                    id="floatingInputCustom"
                    type="text"
                    placeholder="Name"
                    value = {name}
                    onChange={event => setName(event.target.value)}
                    />
                    <label htmlFor="floatingInputCustom">Product Name</label>
                </Form.Floating>
                <Form.Floating className="mb-3">
                    <Form.Control
                    id="floatingPasswordCustom"
                    type="text"
                    placeholder="Description"
                    value = {description}
                    onChange={event => setDescription(event.target.value)}
                    />
                <label htmlFor="floatingPasswordCustom">Description</label>
            </Form.Floating>
            <Form.Floating>
                <Form.Control
                id="floatingPasswordCustom"
                type="number"
                placeholder="Price"
                value = {price}
                onChange={event => setPrice(event.target.value)}
                />
                <label htmlFor="floatingPasswordCustom">Price</label>
            </Form.Floating>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={updateProduct}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>  
          
    </Container>    
   
  );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
// ProductCard.propTypes = {
//   course: PropTypes.shape({
//     name:PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }
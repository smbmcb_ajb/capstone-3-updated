import UserCard from './UserCard';
// import Loading from '../components/Loading'
import {useEffect, useState} from 'react'
import {Table, Accordion, Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
// import courses_data from '../data/courses';

export default function AdminProducts(){
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [error, setError] = useState('')
    const [display, setDisplay] = useState('')
    const [userSearch, setUserSearch] = useState("")
	const [products,setProducts] = useState([])
	// const [isLoading, setIsLoading] = useState(false)

    useEffect(()=> {
		if(name !== "" || description !== "" || price !== ""){
            setDisplay("none")
        }
	},[name, description, price])

    useEffect(()=> {
		// Set the loading state to true
		// setIsLoading(true)
		if(userSearch !== ""){
			// console.log(userSearch)
			fetch(`${process.env.REACT_APP_API_URL}/users/search-user`, {
				method: 'POST',
				headers:{
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					search: userSearch
				})
			})
			.then(response => response.json())
			.then(result => {
				// console.log(result)
				
				setProducts(
					result.map(product => {
						return (
								<UserCard key={product._id} product={product}/>
							
						)
					})
				)
				// Sets the loading state to false
				// setIsLoading(false)
			})
		} else
		{
			fetch(`${process.env.REACT_APP_API_URL}/users`, {
				headers:{
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(response => response.json())
			.then(result => {
				// console.log(result)
				
				setProducts(
					result.map(product => {
						return (
								<UserCard key={product._id} product={product}/>
							
						)
					})
				)
				// Sets the loading state to false
				// setIsLoading(false)
			})
		}
	},[userSearch])

	
	const searchUser = (e)=> {
	setUserSearch(e.target.value)
	// console.log(userSearch)
	// console.log(e.target.value)
	
}
  

	return(
        
		// (isLoading) ?
		// 	<Loading/>
		// :<>
		<>
			<div className='search-users'><input placeholder='Search by email' onChange={searchUser} value={userSearch} className='search-users-input' style={{height:'40px', width:'50%', padding:'7px', outline:'none', marginLeft:'auto', marginRight:'auto', borderRadius:'5px', border: '.5px solid blue'}} type='search'></input></div>
			<Container className = "admin-users-page">
               

			   <Table striped bordered style={{boxShadow: '0px 2px 10px 0px gray'}}>
			   <thead>
				   <tr style={{fontSize:'18px'}}>
				   <th style={{width:'20%'}}>ID</th>
				   <th style={{width:'20%'}}>Name</th>
				   <th style={{width:'22%'}}>Email</th>
				   <th style={{width:'10%'}}>User-type</th>
				   <th style={{width:'10%', wordBreak: 'break-all'}}>Registration date</th>
				   <th style={{width:'14%'}}>Operation</th>
				   </tr>
			   </thead>
			   
			   </Table>
			   {products}
		   </Container>
		</>
			
			
	)
}
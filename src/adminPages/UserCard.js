import {useState, useEffect} from 'react';
import Button from 'react-bootstrap/Button';
import {Card, Container, Modal, Row, Table, Form} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link, NavLink} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function ProductCard({product}) {
    // console.log(product)
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [id, setId] = useState('')
    const [error, setError] = useState('')
    const [display, setDisplay] = useState('')
    const [textMessage, setTextMessage] = useState([])
    const [messages, setMessages] = useState([]);
    const [userId, setUserId] = useState(product._id)
    
    
  // Destructuring the props
//   const {name, description, price, _id} = product
    // setInterval(()=> {
    //     retrieveMessages()
    // },2000)
  // Using the state
  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0)
  const [slot, setSlot] = useState(15)
  const [isOpen, setIsOpen] = useState(true)
const [autoScroll, setAutoScroll] = useState(false)
  const [show, setShow] = useState(false);

  useEffect(()=> {
    if(name !== "" || description !== "" || price !== ""){
        setDisplay("none")
    }
    },[name, description, price])

  const handleClose = () => setShow(false);
  const handleShow = (e) => {
    
        setShow(true)
        const buttonValue = e.target.value;
        setId(buttonValue)
        
        setName(product.name)
        setDescription(product.description)
        setPrice(product.price)
        
    };
    
    const showChatModal = () => {
        // retrieveMessages()
        
        setUserId(product._id)
        setShow(true)
        // console.log(userId)
        
        setTimeout(()=> {
            document.querySelector('#incoming-message').scrollTo(0,document.querySelector('#incoming-message').scrollHeight)
        },2000)
        setTimeout(()=> {
            retrieveMessages()
        },1000)
    }
    
// console.log(userId)

    const sendMessage = ()=> {
  
        fetch(`${process.env.REACT_APP_API_URL}/messages/send-message/${userId}`, {
          method: 'POST',
                headers:{
            'Content-Type':'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
          body: JSON.stringify({
            message: textMessage
          })
            })
            .then(response => response.json())
            .then((result)=> {
                // console.log(result)
          setTextMessage('')
          
          document.getElementById('textarea').innerText = ""
          
          setTimeout(()=> {
            document.querySelector('#incoming-message').scrollTo(0,document.querySelector('#incoming-message').scrollHeight)
        },1000)
        })
      }

      
const detectScroll = (e) => {
    // if (document.getElementById('incoming-message').scrollTop <= (document.getElementById('incoming-message').scrollHeight - (document.getElementById('incoming-message').clientHeight*1)) && (document.getElementById('incoming-message').scrollTop >= 0)){
    //     console.log(document.getElementById('incoming-message').scrollTop)
    //     // console.log(document.getElementById('incoming-message').scrollHeight)
    //     // console.log(document.getElementById('incoming-message').clientHeight)
    //     // alert('scrolling up')
    //     console.log("scrolling up")
    //     setAutoScroll(true)
    // } else 
    // {
    //     // console.log("bottom")
    //     setAutoScroll(false)
    // }
    // console.log(autoScroll)

    
}
      

      const retrieveMessages = () =>{
        fetch(`${process.env.REACT_APP_API_URL}/messages/get-messages/${userId}`, {
            method: 'GET',
            headers:{
              Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
          .then(response => response.json())
          .then(result => {
            // console.log(result)
            if(!result.auth){
            //   console.log(autoScroll)
              setMessages(result)
            //   console.log(messages)
            }
            if(autoScroll === false) {
                document.querySelector('#incoming-message').scrollTo(0,document.querySelector('#incoming-message').scrollHeight)
               }
          })
      }

    useEffect(()=> {
        if(show===true){
            fetch(`${process.env.REACT_APP_API_URL}/messages/get-messages/${userId}`, {
                method: 'GET',
                headers:{
                  Authorization: `Bearer ${localStorage.getItem('token')}`
                }
              })
              .then(response => response.json())
              .then(result => {
                // console.log(result)
                if(!result.auth){
                //   console.log(result)
                  setMessages(result)
                //   console.log(messages)
                
                //    if(autoScroll === false) {
                //     document.querySelector('#incoming-message').scrollTo(0,document.querySelector('#incoming-message').scrollHeight)
                //    }
                // console.log(autoScroll)
                }
                
              })

        }
      })
// console.log(show)
    var d = new Date(product.registeredOn)

    const changeToAdmin = (e) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${e.target.value}/change-usertype`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
       
        })
        .then(response => response.json())
        .then(result => {
            if(result.status === "success"){
                
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: result.message
                })
    

            } else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: result.message
                })
            }
        })
    }

    const resetPassword = (e) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${e.target.value}/reset-password`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
       
        })
        .then(response => response.json())
        .then(result => {
            if(result.status === "success"){
                
                
                Swal.fire({
                    title: `New password: ${result.new_password}`,
                    icon: 'success',
                    text: result.message
                })
    

            } else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: result.message
                })
            }
        })
    }

//   console.log(product.isActive)
  return (
    
    <Container style={{margin: 'auto',width:'100%'}}>
        
    
       <Row>
        <Table striped bordered style={{boxShadow: '0px 2px 10px 0px gray'}}>
                <tbody stye={{padding:'0'}}>
                
                    <tr className='admin-usercard-page' style={{fontSize:'18px', width:'100%',backgroundColor:'white'}}>
                        <td style={{width:'20%', wordBreak: 'break-all'}}>{product._id}</td>
                        <td style={{width:'20%', wordBreak: 'break-all'}}>{product.firstName} {product.lastName}</td>
                        <td style={{width:'22%', wordBreak: 'break-all'}}>{product.email}</td>
                        <td style={{width:'10%'}}>
                            {
                                (product.isAdmin === true) ?
                                    <h6 style={{color: 'red'}}>Admin</h6>
                                :
                                    <h6>User</h6>
                            }
                        </td>
                        <td style={{width:'10%', fontSize:'10px', wordBreak: 'break-all'}}>{d.toLocaleString()}</td>
                        <td style={{width:'14%'}}>
                            {
                                (product.isAdmin === true) ?
                                    <></>
                                :
                                    <>
                                        {
                                            (localStorage.getItem('csr') === "true") ?
                                            <Button className="my-2 admin-product-operation" id='open-chat-btn' value={product._id} variant="primary" style={{width:'70%', fontSize:'12px', paddingY:'0',paddingX:'auto', textAlign:'center', display:'flex',justifyContent:'center'}} onClick={showChatModal}>Open Convo</Button>
                                        :
                                           <>
                                                <Button className="my-2 admin-product-operation" value={product._id} variant="primary" onClick={changeToAdmin} style={{width:'70%', fontSize:'12px', paddingY:'0',paddingX:'auto', textAlign:'center', display:'flex',justifyContent:'center'}}>Promote to Admin</Button>
                                                <Button className="my-2 admin-product-operation" value={product._id} variant="primary" onClick={resetPassword} style={{width:'70%', fontSize:'12px', paddingY:'0',paddingX:'auto', textAlign:'center', display:'flex',justifyContent:'center'}} >Reset Password</Button>
                                           </>
                                        
                                        }
                                        
                                    </>
                            }
                           
                            
                            
                        </td>
                    </tr>
                
                </tbody>
        
        </Table>
      </Row>
        
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Sending Message to: {product.firstName}</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{height: '25rem', display:'block' }}>
            <div onScroll={detectScroll} id="incoming-message"  style={{height:'100%', border: '1px solid gray', display: 'flex', flexDirection: 'column', overflowY: "scroll"}} >
                {
                    messages.map((m, index)=> (
                       <div key={index} >
                             <div style={{display: 'flex', flexDirection: 'column'}}>
                                {
                                    m.to  ?
                                        <><center style={{fontSize: '13px'}}>{(new Date(m.sentOn)).toLocaleString()}</center>
                                            <div style={{minHeight:'3rem' ,maxHeight: '25rem' ,  alignSelf:"flex-start", marginLeft:'7px', border:'1px solid gray', borderRadius: '10px 10px 10px 1px', marginLeft:'7px'}}>
                                                <div style={{minWidth:'3rem', maxWidth: '16rem', alignSelf:"flex-start", paddingTop:'7px', paddingLeft:'12px', paddingRight:'12px'}}>
                                                    <div style={{overflowWrap:'break-word'}}>{m.to}</div>
                                                </div>
                                            </div>
                                        </>
                                    :
                                        <></>
                                }
                                
                            </div>
                            <div style={{display: 'flex', flexDirection: 'column'}}>
                                {
                                    m.from ?
                                        <><center style={{fontSize: '13px'}}>{(new Date(m.sentOn)).toLocaleString()}</center>
                                            <div style={{minHeight:'3rem', maxHeight: '25rem', border:'1px solid gray', borderRadius: '10px 10px 1px 10px', alignSelf:"flex-end", marginRight:'7px', marginBottom:'5px'}}>
                                                <div style={{minWidth:'3rem', maxWidth: '16rem', alignSelf:"flex-end", textAlign:"right", paddingTop:'7px', paddingLeft:'12px', paddingRight:'12px' }}>
                                                    <div style={{overflowWrap:'break-word'}}>{m.from}</div>
                                                </div>
                                            </div>
                                        </>
                                    :
                                        <></>
                                    }
                            </div>
                       </div>
                    ))
                }
                
                
            </div>
        </Modal.Body>
        <Modal.Footer>
        <div style={{width: "100%", height:'50px', display:'block'}}>
        <span style={{width: "65%", marginBottom: '10px'}} id='textarea' className='textarea' role='textbox' contentEditable onSelect={(e) => setTextMessage(e.target.innerText) } ></span>
        <Button onClick={sendMessage} value={textMessage} style={{float: 'right', marginTop: '5px', width: '28%'}} variant="success">
            Send
        </Button>
        </div>
          
        </Modal.Footer>
      </Modal> 
                            
    </Container>    
   
    

  );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them
// ProductCard.propTypes = {
//   course: PropTypes.shape({
//     name:PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }
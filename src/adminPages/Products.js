import ProductCard from './ProductCard';
// import Loading from '../components/Loading'
import {useEffect, useState} from 'react'
import {Table, Accordion, Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
// import courses_data from '../data/courses';

export default function AdminProducts(){
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [error, setError] = useState('')
    const [display, setDisplay] = useState('')
    const [prodName, setProdName] = useState("")
	const [products,setProducts] = useState([])
	// const [isLoading, setIsLoading] = useState(false)

    useEffect(()=> {
		if(name !== "" || description !== "" || price !== ""){
            setDisplay("none")
        }
	},[name, description, price])

    // useEffect(()=> {
	// 	// Set the loading state to true
	// 	// setIsLoading(true)
	// 	fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
    //         headers:{
	// 			Authorization: `Bearer ${localStorage.getItem('token')}`
	// 		}
    //     })
	// 	.then(response => response.json())
	// 	.then(result => {
            
	// 		setProducts(
	// 			result.map(product => {
	// 				return (
	// 						<ProductCard key={product._id} product={product}/>
						
	// 				)
	// 			})
	// 		)
	// 		// Sets the loading state to false
	// 		// setIsLoading(false)
	// 	})
	// })
   
    const addProduct = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name:name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result.status === "success"){
                
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: result.message
                })

                setName('')
                setDescription('')
                setPrice('')
            } else {
                setDisplay("flex")
                setError(result.message)
            }
        })
    }    

    
	const searchProduct = (e)=> {
	setProdName(e.target.value)
	// console.log(prodName)
	// console.log(e.target.value)
}
useEffect(()=> {
	if(prodName !== ""){
		// console.log(prodName)
	// console.log('searching')
	fetch(`${process.env.REACT_APP_API_URL}/products/search`,{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			prodName: prodName
		})
	})
		.then(response => response.json())
		.then(result => {
			setProducts(
				result.map(product => {
					return (
							<ProductCard key={product._id} product={product}/>
						
					)
				})
			)
			// Sets the loading state to false
			// setIsLoading(false)
		})
	}
	else
	{
        // console.log(prodName)
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(response => response.json())
		.then(result => {
			setProducts(
				result.map(product => {
					return (
							<ProductCard key={product._id} product={product}/>
						
					)
				})
			)
			// Sets the loading state to false
			// setIsLoading(false)
		})
	}
},[prodName])
	
	return(
        
		// (isLoading) ?
		// 	<Loading/>
		// :
			<div className = "product-container">
                <Accordion className="text-center w-50 admin-product-accordion" style={{boxShadow: '0px 2px 10px 0px gray', maxHeight:'400px'}}>
                    <Accordion.Item eventKey="1">
                    <Accordion.Header>Add New Product</Accordion.Header>
                        <Accordion.Body>
                            <Form.Floating className="mb-3">
                                <Form.Control
                                id="floatingInputCustom"
                                type="text"
                                placeholder="Name"
                                value = {name}
                                onChange={event => setName(event.target.value)}
                                />
                                <label htmlFor="floatingInputCustom">Product Name</label>
                            </Form.Floating>
                            <Form.Floating className="mb-3">
                                <Form.Control
                                id="floatingPasswordCustom"
                                type="text"
                                placeholder="Description"
                                value = {description}
                                onChange={event => setDescription(event.target.value)}
                                />
                                <label htmlFor="floatingPasswordCustom">Description</label>
                            </Form.Floating>
                            <Form.Floating>
                                <Form.Control
                                id="floatingPasswordCustom"
                                type="number"
                                placeholder="Price"
                                value = {price}
                                onChange={event => setPrice(event.target.value)}
                                />
                                <label htmlFor="floatingPasswordCustom">Price</label>
                            </Form.Floating>
                            <label className='errorMessage-container mt-3' style={{display:display}}>{error}</label>
                            <Button variant="primary" size="lg" onClick={addProduct} className='w-100 mt-3'>Add</Button>
                            
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>

                <div className='admin-search-users'><input placeholder='Search by name or description' onChange={searchProduct} value={prodName} className='search-users-input' style={{height:'40px', width:'50%', padding:'7px', outline:'none', marginLeft:'auto', marginRight:'auto', borderRadius:'5px', border: '.5px solid blue'}} type='search'></input></div>

                <Table className='admin-product-table-heading' striped style={{height:'80px'}}>
                <thead>
                    <tr className="admin-product-table" style={{fontSize:'16px'}}>
                    <th style={{width:'20%'}}>ID</th>
                    <th style={{width:'15%'}}>Name</th>
                    <th style={{width:'35%', wordBreak: 'break-all'}}>Description</th>
                    <th style={{width:'10%'}}>Price</th>
                    <th style={{width:'10%'}}>Availability</th>
                    <th className='admin-product-operation-heading' style={{width:'10%', wordBreak: 'break-all'}}>Operation</th>
                    </tr>
                </thead>
				
                </Table>
                {products}
			</div>
			
	)
}
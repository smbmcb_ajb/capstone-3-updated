import OrderCard from '../components/OrderCard';
import {useEffect, useState} from 'react'
import Radio from '@mui/material/Radio';
import FormControlLabel from '@mui/material/FormControlLabel';
import RadioGroup from '@mui/material/RadioGroup';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

export default function Order(){
	const [orders,setOrders] = useState([])
	const [value, setValue] = useState('history');
	
  // console.log(orders)
	// const [isLoading, setIsLoading] = useState(false)

	useEffect(()=> {
    // console.log(localStorage.getItem('token'))
		// Set the loading state to true
		// setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/orders/${value}`, {
      headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
    })
		.then(response => response.json())
		.then(result => {
    //   console.log(result)
			if(result.message !== "No orders yet.") {
				setOrders(
					result.map(order => {
						
						return (
								<OrderCard key={order._id} order={order}/>
							
						)
					})
				)
			} else {
				setOrders(
						
				<center><h1>No orders yet</h1></center>
				)	
			}
			
			// Sets the loading state to false
			// setIsLoading(false)
		})
	},[value])

	const handleChange = (event) => {
		setValue(event.target.value);
	}
	// console.log(orders)

	return(
		// (isLoading) ?
		// 	<Loading/>
		// :
			<div className='mt-4'>
				<FormControl>
				<FormLabel id="demo-row-radio-buttons-group-label">Search by status</FormLabel>
					<RadioGroup
						row
						aria-labelledby="demo-row-radio-buttons-group-label"
						name="row-radio-buttons-group"
						value={value}
    					onChange={handleChange}
					>
						<FormControlLabel value="history" control={<Radio />} label="all" />
						<FormControlLabel value="history/pending" control={<Radio />} label="pending" />
						<FormControlLabel value="history/for-delivery" control={<Radio />} label="for-delivery" />
						<FormControlLabel value="history/completed" control={<Radio />} label="completed" />
						<FormControlLabel value="history/cancelled" control={<Radio />} label="cancelled" />
					</RadioGroup>
					</FormControl>
				{orders}
      		</div>
			
	)
}
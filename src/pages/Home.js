import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import {Link, NavLink} from 'react-router-dom'
import {Nav, Container, Row, Col, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import background from '../images/quality.jfif'


export default function Home(){
    

    return(
        <Container id="home-container" fluid style={{height: '100%', width:'80%',  paddingTop: '90px'}}>
            {
                (!localStorage.getItem('token')) ?
                    <center><NavLink style={{textDecoration:'none', color: 'black', fontWeight:'bold'}} to='/register'>
                            <Button variant='success' size='lg' className='w-50 sign-up-btn'>SignUp Now</Button>
                        </NavLink>
                    </center>
                :
                    <></>
            }
            <Row style={{height: '30%', display: 'flex', justifyContent: 'center', alignItems: 'center', marginLeft:'auto', marginRight:'auto'}}>
                <div className='home-banner-mini' id='home-banner-mini' style={{ height: '70%', display:'flex',justifyContent:'center',alignItems:'center', width: '30%', boxShadow: '0px 2px 10px 0px gray', borderRadius:'10px', overflow:'hidden'}}>
                    <img className='home-banner-img-1' src='./hand-money.png' style={{width: '60px'}}></img><span className='affordable' style={{fontSize: '26px'}}>Affordable</span><img className='home-banner-img-2' src='./pot-money.jfif' style={{width: '60px'}}></img>
                    
                </div>
                <div className='divider-mini-banner' style={{width: '5%'}}></div>
                
                <div className='home-banner-mini' style={{backgroundImage: `url(${background})`,  height: '70%', padding:'1rem', width: '30%', boxShadow: '0px 2px 10px 0px gray', borderRadius:'10px'}}></div>
                <div className='divider-mini-banner' style={{width: '5%'}}></div>
                <div className='home-banner-mini' style={{ height: '70%', width: '30%', boxShadow: '0px 2px 10px 0px gray', borderRadius:'10px',overflow:'hidden'}}>
                    <img className='home-banner-img' src='./delivery.jfif' style={{width: '100%', height: '100%'}}></img>
                </div>
            </Row>
            <Row className='home-banner' style={{height: '62%'}}><img className='img-home-banner'  src='./home.png' style={{width: '100%', height: '100%', padding:'0'}}></img></Row>
           
        </Container>
    )
}
import ProductCard from '../components/ProductCard';
// import Loading from '../components/Loading'
import {useEffect, useState} from 'react'
// import courses_data from '../data/courses';

import {Form} from 'react-bootstrap'

export default function Products(){
	const [products,setProducts] = useState([])
	const [prodName, setProdName] = useState("")
	// const [isLoading, setIsLoading] = useState(false)

	// useEffect(()=> {
	// 	// Set the loading state to true
	// 	// setIsLoading(true)
	// 	fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	// 	.then(response => response.json())
	// 	.then(result => {
	// 		setProducts(
	// 			result.map(product => {
	// 				return (
	// 						<ProductCard key={product._id} product={product}/>
						
	// 				)
	// 			})
	// 		)
	// 		// Sets the loading state to false
	// 		// setIsLoading(false)
	// 	})
	// })


	
const searchProduct = (e)=> {
	setProdName(e.target.value)
	// console.log(prodName)
	// console.log(e.target.value)
	
}

useEffect(()=> {
	if(prodName !== ""){
		// console.log(prodName)
	// console.log('searching')
	fetch(`${process.env.REACT_APP_API_URL}/products/search`,{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			prodName: prodName
		})
	})
		.then(response => response.json())
		.then(result => {
			setProducts(
				result.map(product => {
					return (
							<ProductCard key={product._id} product={product}/>
						
					)
				})
			)
			// Sets the loading state to false
			// setIsLoading(false)
		})
	}
	else
	{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(response => response.json())
		.then(result => {
			setProducts(
				result.map(product => {
					return (
							<ProductCard key={product._id} product={product}/>
						
					)
				})
			)
			// Sets the loading state to false
			// setIsLoading(false)
		})
	}
},[prodName])

	return(
		// (isLoading) ?
		// 	<Loading/>
		// :
		<>
			<Form className="d-flex form-search-product"  style={{paddingTop:'8rem', marginRight:'auto', marginLeft:'auto'}} >
                        <Form.Control
						onChange={searchProduct}
                        type="search"
                        placeholder="Search by name or description"
                        className="me-2 search-product"
                        aria-label="Search"
                        style={{border: '1px solid #3746EC', width:'94%'}}
						value={prodName}
                        />
    		</Form>
			<div className = "product-container">
				{products}
			</div>
		</>
			
	)
}
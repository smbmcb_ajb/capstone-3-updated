import { useEffect, useState } from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import {Container, Row, Table, Form, Button} from 'react-bootstrap'
import CartCard from '../components/CartCard'
import Swal from 'sweetalert2'



export default function Cart(){
    const navigate = useNavigate()
    const [cart, setCart] = useState([])
    const [totalAmount, setTotalAmount] = useState()

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
        
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
        .then(response => response.json())
        .then(result => {
            if(result.status !== "error"){
                setTotalAmount(result.cart.totalAmount)
                // console.log(totalAmount)
                setCart(
                    result.cart.products.map(product => {
                        
                        return (
                            <CartCard key={product._id} product={product}/>
                        )
                    })
                    )
    
            } 
        })
    })

    const checkout = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/check-out`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result.status === "success"){
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: result.message
                })
                navigate('/account')
               
            } else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: result.message
                })
                navigate('/products')
            }
        })
    }
    // console.log(cart)
    return(
        <>
        <Container style={{height: '84%', width: '80%', overflowY:'auto'}}>
            <Row className='cart-row' style={{height: '200px'}}>
                <div className='cart-desc-container' style={{
                    display: 'flex'
                }}>
                    <div className='w-50 cart-shopping-container' style={{paddingTop:'7rem', color: '#3746EC'}}>Shopping Cart</div>
                    {/* <Form className="d-flex w-50" style={{paddingTop:'8rem', display:'none'}} >
                        <Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2 h-50"
                        aria-label="Search"
                        style={{border: '1px solid #3746EC'}}
                        />
                        <Button className='h-50' variant="primary" style={{display:'flex',justifyContent:'center', alignItems:'center'}}>Search</Button>
                    </Form> */}
                </div>
                  
            </Row>
            <Row>
            <Table>
                    <thead className='user-product-heading' style={{boxShadow: '0px 2px 10px 0px gray', height: '5rem'}}>
                        <tr style={{fontSize:'18px'}}>
                            <th style={{width:'44%'}}>Product</th>
                            <th className='text-center' style={{width:'14%'}}>Price</th>
                            <th className='text-center' style={{width:'14%'}}>Quantity</th>
                            <th className='text-center' style={{width:'14%'}}>Sub-total</th>
                            <th className='text-center' style={{width:'14%'}}>Actions</th>
                        </tr>
                    </thead>				
                </Table>
                {
                    (cart.length === 0) ?
                    <center><h1>Your shopping cart is empty</h1></center>
                    :
                        cart
                }
                
                
                
            </Row>
                
        </Container>
        <Container style={{
                
                width: '80%',
                height: '15%',
                bottom: '1px',
                position: 'fixed',
                left: '10%'
                
                
            }}>
                <div style={{display: 'flex', width: '100%'}}>
                    <Button style={{
                        marginTop: '3%',
                        marginLeft: '3%',
                        width: '60%'
                    }} variant='success' size='lg' onClick={checkout}>Check out</Button>
                    <label style={{
                        marginTop: '3%', 
                        marginLeft: '10%',
                        width: '40%',
                        fontSize: '20px'
                    }}>Total Amount: <span style={{fontSize: '24px', color: 'red'}}>₱ {totalAmount}</span></label>
                </div>
            
            </Container>
        </>
            
    )
}
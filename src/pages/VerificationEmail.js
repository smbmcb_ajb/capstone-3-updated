import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'





export default function VerificationEmail(){
    const [message,setMessage] = useState('');
    const {email} = useParams();
    const {passphrase} = useParams();
    // console.log(email)

    fetch(`${process.env.REACT_APP_API_URL}/users/verify/${email}/${passphrase}`, {
        method: 'GET'
        
    })
    .then(response => response.json())
    .then(result => {
        // console.log(result)
        if(result.status === "success"){
            setMessage('success');
            Swal.fire({
                title: 'Success',
                icon: 'success',
                text: result.message
            })
        } else
        
        if(result.status === "error"){
            setMessage('error');
            Swal.fire({
                title: 'Error',
                icon: 'error',
                text: result.message
            })
        } 
        
        else {
            Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'Something went wrong.'
            })
        }
    })
    return(
        <div 
        style={{
            marginTop: '15%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
        }}
        >
            <img src='/ecommerce.png' style={{width:'100px', height:'100px'}}></img>
           {
             message === "success" ?
                <p style={{fontWeight:'bold', fontSize:'12px'}}>Thank you for subscribing</p>
            :
                <p style={{fontWeight:'bold', fontSize:'12px'}}>Verification Unsuccessful</p>
           }

            
            <div style={{lineHeight:'.1', textAlign:'center'}}>
                {
                    message === "success" ?
                        <>
                            <p style={{fontSize:'12px'}}>Welcome to OnelineTindahan</p>
                            <p style={{fontSize:'12px'}}>We are looking forward to having you buy with us.</p>
                        </>
                    :
                        <p style={{fontSize:'12px'}}>Unable to proceed</p>
                }
            </div>
            
        </div>
    )
}

    

import {useState, useEffect,useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import {Link, NavLink} from 'react-router-dom'
import Nav from 'react-bootstrap/Nav'


export default function Register(){
    

    const navigate = useNavigate()

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('') 
    const [error, setError] = useState('')
    let [display, setDisplay] = useState('')
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
	

    useEffect(()=> {
		if(email !== "" || password1 !== "" || password2 !== "" || firstName !== "" || lastName !== ""){
            setDisplay("none")
        }
	},[email, password1, password2, firstName, lastName])


    const sign_up = (e) => {
        e.preventDefault()
    
        if(email === "" || email === null || password1 === "" || password1 === null || password2 === "" || password2 === null || firstName === "" || firstName === null || lastName ==="" || lastName === ""){
            setError("All Fields are required!")
            setDisplay("flex")
        } 
        else 
            if(!email.match(mailformat)){
                setDisplay("flex")
                setError("Invalid email format!")
            } 
        else 
            if(!password1.match(password2)){
                setDisplay("flex")
                setError("Passwords dont match!")
            }
         
        else {
            
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email:email,
                    password: password1,
                    firstName: firstName,
                    lastName: lastName,
                    
                })
            })
            .then(response => response.json())
            .then(result => {
                // console.log(result)
                if(result.message === 'duplicate'){
                    // console.log("sdfsdf")
                    setDisplay("flex")
                    setError(`Duplicate Email!. ${email} is already in use`)
                } else
                if(result === 'false') {
                    setDisplay("flex")
                    setError("Something went wrong!")
                } else {
                Swal.fire({
                    title: 'Registration Successful!',
                    icon: 'success',
                    text: result.message
                })
                navigate('/login')
                }  
            })
        
        }
    }

	// for determining if button is disabled or not
	// const [isActive, setIsActive] = useState(false)
    
    if(localStorage.getItem('token')){
        return(
            <Navigate to='/products'/>
        )
    }

    return(
        <div className='content' id="body">

            <div className="logo">
                <center>
                    {/* <h1>Online-Tindahan</h1> */}
                    <img className = "logo_login mt-5" src="/ontindahan3.png" alt="company logo" />
                    
                </center>
            </div>
        
            <div className="register">
                <center><h3>Account Sign-up</h3></center>
                
                
                <form action="#" method="POST">
                    <div className="username-signup-field">
                        <label htmlFor="firstName" className="label-signup-field">First Name</label><br />
                        <input id="firstName" type="text" name="firstName" onChange={event => setFirstName(event.target.value)} value={firstName} className="input-signup-field" autoComplete="true" required />
                    </div><div className="username-signup-field">
                        <label htmlFor="lastName" className="label-signup-field">Last Name</label><br />
                        <input id="lastName" type="lastName" name="lastName" onChange={event => setLastName(event.target.value)} value={lastName} className="input-signup-field" autoComplete="true" required />
                    </div>
                    <div className="username-signup-field">
                        <label htmlFor="email" className="label-signup-field">Email</label><br />
                        <input id="email" type="email" name="email" onChange={event => setEmail(event.target.value)} value={email} className="input-signup-field" autoComplete="true" required />
                    </div>
                    
                    <div className="username-signup-field">
                        <label htmlFor="password1" className="label-signup-field">Password</label><br />
                        <input id="password1" type="password" name="password1" onChange={event => setPassword1(event.target.value)} value={password1} className="input-signup-field" />
                        
                    </div>
                    <div className="username-signup-field">
                        <label htmlFor="password2" className="label-signup-field">Verify Password</label><br />
                        <input id="password2" type="password" name="password2" onChange={event => setPassword2(event.target.value)} value={password2} className="input-signup-field" autoComplete="true" required />
                    </div>
                    
                    
                    <div className="username-signup-field">
                        <label id="register_error" className="register_error" style={{display:display}}>{error}</label><br />
                    </div>

                    <div className="username-signup-field">
                        <button id="sign_up" className="btn-sign-in" type="submit" onClick={sign_up} name="sign-up">Sign-up</button>
                    </div>
                    <center><p>Already have an Account? Log-in <Nav.Link as={NavLink} to="/login" style={{color: 'blue', fontWeight:"bold"}}>Here</Nav.Link></p></center>
                </form>
            </div>
        </div>
    )
}